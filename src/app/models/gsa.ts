export default class GSA {
	selectionMode: string;
  mode: number;
  satellites: string[];
  PDOP: string;
  HDOP: string;
  VDOP: string;

	constructor(data) {
		this.selectionMode = data.selectionMode;
	  this.mode = data.mode;
	  this.satellites = data.satellites;
	  this.PDOP = data.PDOP;
	  this.HDOP = data.HDOP;
	  this.VDOP = data.VDOP;
	}
}