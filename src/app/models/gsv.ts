export default class GSV {
  numMsgs: number;
  msgNum: number;
  satsInView: number;
  satellites: string[];

  constructor(data) {
  	this.numMsgs = data.numMsgs;
  	this.msgNum = data.msgNum;
  	this.satsInView = data.satsInView;
  	this.satellites = data.satellites;
  }
}