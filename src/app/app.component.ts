import { Component, OnInit } from '@angular/core';
import { GpsService } from './services/gps.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	coords: Promise<string[]>;

	// Обращение к сервису как this.gpsService
  constructor (private gpsService: GpsService) {}

  // Обращаемся к методу getCoords из сервиса gpsService и передаем данные в переменную this.coords
  getCoords(): void {
    this.coords = this.gpsService.getCoords();
  }

  ngOnInit() {
  	this.getCoords();
  }
}