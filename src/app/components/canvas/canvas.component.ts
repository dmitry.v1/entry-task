import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';
import Canvas from '../../models/canvas';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit {

  constructor() { }

  canvasCreate() {
  	const canvas = document.getElementById('canvas');
  	const ctx = canvas.getContext('2d');
  	const rect = canvas.getBendingClientRect();
  	const scale = window.divicePixelRatio;

  	canvas.width = rect.width * scale;
  	canvas.height = rect.height * scale;
  	ctx.scale(scale, scale);

  	const mouseMove$ = fromEvent(canvas, 'mousemove');

  	mouseMove$
  		.pipe(
  			map(e => ({
  				x: e.offsetX,
  				y: e.offsetY,
  			})),
  			pairwise()
  		)
  		.subscribe(pos => {
  			console.log(pos);
  			ctx.fillRect(pos.x, pos.y, 2, 2);
  		})
  }

  ngOnInit(): void {
  }

}
