import { Component, OnInit } from '@angular/core';

import { GpsService } from '../../services/gps.service';


@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

	disabledStart: boolean = this.gpsService.disabledStart;
	disabledStop: boolean = this.gpsService.disabledStop;

	constructor(private gpsService: GpsService) {}

  // Запускаем сообщения
  start(): void {
  	if (this.gpsService.stopped) {
  		this.gpsService.getGpsLog();
	  	this.gpsService.stopped = false;
	  	this.disabledStart = true;
	  	this.disabledStop = false;
	  	this.gpsService.request = setInterval(() => this.gpsService.timeRepeat(), this.gpsService.time);
  	}
  }

  // Останавливаем сообщения
  stop(): void {
  	this.clearInterval();
  	this.gpsService.stopped = true;
  	this.disabledStart = false;
  	this.disabledStop = true;
  }

  // Останавливаем сообщения
  clearInterval(): void {
  	clearInterval(this.gpsService.request);
  }

  ngOnInit(): void {
  }

}
