import { Injectable } from '@angular/core';
import GSV from '../models/gsv';

@Injectable({
  providedIn: 'root'
})
export class GetGSV {
	constructor() {}

	getData(fields: string[]) {
		let numRecords = (fields.length - 4) / 4;
		let sats = [];
		let rules = [];

		for (let i = 0; i < numRecords; i++) {
	    let offset = i * 4 + 4;
	    sats.push({
	    	id: fields[offset],
	      elevationDeg: fields[offset + 1],
	      azimuthTrue: fields[offset + 2],
	      SNRdB: fields[offset + 3],
	    });
	  }

	  rules.push({
	    numMsgs: fields[1],
	    msgNum: fields[2],
	    satsInView: fields[3],
	    satellites: sats,
	  });

	  return rules;
	}

}
