import { Injectable } from '@angular/core';
import GSA from '../models/gsa';
import GSV from '../models/gsv';
import { GetGSV } from './gsv.service';

@Injectable({
  providedIn: 'root'
})
export class GpsService {	
	public position: number = 0;
	public count: number = 4;
	public time: number = 1000;
	public log: string[];
	public stopped: boolean = true;
	public request: any;
	public disabledStart: boolean = false;
	public disabledStop: boolean = true;
  public parseValues: GSV[];

  public apiHost = '/assets/GPS_log.txt';
	public coords: Promise<any>;

	constructor(
		private getGSV: GetGSV,
	){
		this.coords = fetch(this.apiHost)
    	.then(response => response.text())
    	.then(response => {
	    	localStorage.setItem('gps_log', JSON.stringify(response.split('\r\n')));
    	});
	}

	getCoords() {
		return this.coords;
	}

  // Получаем данные из localStorage
  getGpsLog(): void {
  	this.log = JSON.parse(localStorage.getItem('gps_log'));
  }

  // Получаем контрольную сумму
  getCheckSum(string): string {
  	return string.split('*')[1];
  }

  // Получаем данные строки
  getDataString(string): string[] {
  	return string.split('*')[0].split(',');
  }

  // Получаем часть аббревиатуры у строки
  getAbbr(string): string {
  	let fields: string[] = string.split('*')[0].split(',');
  	let abbr: string = fields[0].substr(-3, 3);
  	return abbr;
  }

  // Получаем распарсенные значения
  getValues(datas: string[]): void {
    this.parseValues = this.getGSV.getData(datas).map(item => item);
  }

  // Парсер строки
  parseStrings(): void {
  	const startPos = this.position;
  	const endPos = startPos + this.count;

  	for (let i = startPos; i < endPos; i++) {
  		let abbr = this.getAbbr(this.log[i]);
  		let datas = this.getDataString(this.log[i]);

      this.getValues(datas);
      console.log(this.parseValues);
  	}
  }

  // Парсер строки
  timeRepeat(): void {
	  this.parseStrings();
  	this.position += this.count;
  }

}
